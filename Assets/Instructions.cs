﻿using UnityEngine;
using System.Collections;

public class Instructions : MonoBehaviour {
	void OnGUI() {
		GUILayout.BeginArea(new Rect(0,0,150,200));
		GUILayout.Box("WASD: move\nSpace: jump\nMouse: aim\nClick: shoot");
		GUILayout.EndArea();
	}
}
