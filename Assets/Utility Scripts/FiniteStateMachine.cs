﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;


namespace CoderFred
{
	public delegate IEnumerator CoroutineDelegate();

	public abstract class FiniteStateMachine : State
	{
		State startingState;

		protected override void Awake()
		{
			base.Awake();
			StateSetup();
		}

		protected virtual void OnEnter()
		{
			foreach (var state in states)
			{
				state.Value.gameObject.SetActive(false);
			}
			current = startingState;
			current.gameObject.SetActive(true);
			current.SendEnter();
		}
		
		protected abstract void StateSetup();

		protected T AddState<T>() where T : State
		{
			GameObject go = new GameObject(typeof(T).ToString());
			go.SetParent(gameObject);
			go.hideFlags = HideFlags.HideInHierarchy;

			T state = go.AddComponent<T>();

			state.owner = this;

			if (states == null)
			{
				states = new Dictionary<Type, State>();
				startingState = state;
				current = state;
				current.SendEnter();
			}
			else
			{
				state.gameObject.SetActive(false);
			}

			states.Add (typeof(T), state);

			return state;
		}

		Dictionary<Type, State> states;

		State current;

		public T SwitchState<T>() where T : State
		{
			current.SendExit();
			current.gameObject.SetActive(false);
			
			current = states[typeof(T)];
			current.gameObject.SetActive(true);
			current.SendEnter();
			return (T) current;
		}

		public bool CheckState<T>() where T : State
		{
			return current = states[typeof(T)];
		}

		public T GetState<T>() where T : State
		{
			return (T) states[typeof(T)];
		}
	}

	public abstract class State : Behaviour
	{
		public void SendEnter()
		{
			foreach (var kv in tasks)
			{
				kv.Value.OnEnter();
			}
			OnEnter();
		}

		public void SendExit()
		{
			foreach (var kv in tasks)
			{
				kv.Value.OnExit();
			}
			OnExit();
		}

		public virtual void OnEnter() {}
		public virtual void OnExit() {}

		public bool IsActive { get { return gameObject.activeInHierarchy; } }

		public T StateTransition<T>() where T : State
		{
			return owner.SwitchState<T>();
		}
		
		public void Finish() { owner.SendMessage("OnFinish"); }

		public FiniteStateMachine owner { get; set; }
		
		public FiniteStateMachine root
		{
			get
			{
				var i = owner;
				while (i != null && i.owner != null)
				{
					i = i.owner;
				}
				return i;
			}
		}

		#region Tasks

		public virtual void TaskSetup() {}
		
		protected virtual void Awake()
		{
			if (tasks == null) tasks = new Dictionary<Type, Task>();
			TaskSetup();
		}
		
		private Dictionary<Type, Task> tasks;
		
		public T AddTask<T>() where T : Task
		{
			var task = gameObject.AddComponent<T>();
			task.state = this;
			task.hideFlags = HideFlags.HideInInspector;
			tasks.Add(typeof(T), task);
			return task;
		}

		public T GetTask<T>() where T : Task
		{
			return (T) tasks[typeof(T)];
		}

		#endregion
	}

	public abstract class Task : Behaviour
	{
		public virtual void OnEnter() {}
		public virtual void OnExit() {}

		public FiniteStateMachine owner { get { return state.owner; } }

		public State state { get; set; }

		public FiniteStateMachine root { get { return state.root; } }

	}
}