using System;
using UnityEngine;

public static class TransformExtensions
{
	public static void LookAt2D(this Transform transform, Vector2 target)
	{
		Vector2 direction = target - transform.GetPosition2D();
		
		float angle = direction.Direction();
		
		transform.SetRotation2D(angle);
	}

	public static void SetPosition2D(this Transform transform, Vector2 position)
	{
		transform.SetPosition2D(position.x, position.y);
	}

	public static void SetPosition2D(this Transform transform, float x, float y)
	{
		transform.position = new Vector3(x,y, transform.position.z);
	}

	public static Vector2 GetPosition2D(this Transform transform)
	{
		return transform.position.To2D();
	}

	public static void Rotate2D(this Transform transform, float angle)
	{
		transform.Rotate(0f, 0f, angle);
	}

	public static float GetRotation2D(this Transform transform)
	{
		return transform.eulerAngles.z;
	}

	public static void SetRotation2D(this Transform transform, float angle)
	{
		transform.Rotate2D(angle - transform.GetRotation2D());
	}

	public static void Translate2D(this Transform transform, Vector2 translation)
	{
		transform.Translate (translation.To3D());
	}

	public static void SetPositionX(this Transform transform, float x)
	{
		transform.position = new Vector3(x, transform.position.y, transform.position.z);
	}

	public static void SetPositionY(this Transform transform, float y)
	{
		transform.position = new Vector3(transform.position.x, y, transform.position.z);
	}

	public static void SetPositionZ(this Transform transform, float z)
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, z);
	}

	public static float Distance2DTo(this Transform transform, Transform target)
	{
		return (transform.GetPosition2D() - target.GetPosition2D()).magnitude;
	}
}
