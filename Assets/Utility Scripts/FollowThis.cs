﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This Behaviour is meant to be attached to a Camera to make it follow a specific game object in 2D space.
/// </summary>
public class FollowThis : MonoBehaviour {

	public GameObject followThis;

	void LateUpdate () {
		if (followThis != null)
		{
			transform.position = new Vector3 (
				followThis.transform.position.x,
				followThis.transform.position.y,
				transform.position.z
				);
		}
	}
}
