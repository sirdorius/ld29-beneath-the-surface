﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Behaviour : MonoBehaviour {

	#region Component Caching

	Dictionary<System.Type, Component> cache = new Dictionary<System.Type, Component>();

	/// <summary>
	/// Like GetComponent, but actually searches for the component only the first time, then stores the component lazily. 
	/// </summary>
	/// <returns>The required component.</returns>
	/// <typeparam name="T">The type of the component.</typeparam>
	public T LazyComponent<T>() where T : Component
	{
		if (!cache.ContainsKey(typeof(T)))
		{
			cache[typeof(T)] = GetComponent<T>();
		}
		return (T) cache[typeof(T)];

	}

	#endregion

}
