﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameEvent<T> {

	public delegate void Listener(T arg);

	List<Listener> listeners = new List<Listener>();

	public void Subscribe(Listener listener)
	{
		if (!listeners.Contains(listener))
		{
			listeners.Add(listener);
		}
	}

	public void Unsubscribe(Listener listener)
	{
		if (listeners.Contains(listener))
		{
			listeners.Remove(listener);
		}
	}

	public void Dispatch(T arg)
	{
		List<Listener> toRemove = new List<Listener>();
		foreach(Listener l in listeners)
		{
			try
			{
				l(arg);
			}
			// TODO: identify exception to catch
			catch
			{
				toRemove.Add(l);
			}
		}

		foreach(Listener l in toRemove)
		{
			listeners.Remove(l);
		}
	}

}

public struct AnyDamageInfo
{
	public Component sender;
	public int amount;
}

public struct DeathInfo
{
	public Component sender;
}