﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnnulusCollider2D : MonoBehaviour {

	public int numberOfSides = 20;
	private int numberOfSidesOld;

	public Vector2 center = Vector2.zero;
	private Vector2 centerOld;

	public float radiusOutside;
	private float radiusOutsideOld;

	public float radiusInside;
	private float radiusInsideOld;

	public bool isTrigger;
	private bool isTriggerOld;

	public bool hideCollider = true;

	public PhysicsMaterial2D material;
	private PhysicsMaterial2D materialOld;

	private PolygonCollider2D _collider;

	private List<Vector2> points;

	void getCollider()
	{
		if (_collider == null)
		{
			_collider = gameObject.AddComponent<PolygonCollider2D>();
		}
	}

	void Awake()
	{
		getCollider();
		
		points = getPoints(numberOfSides);
		
		_collider.points = points.ToArray();
		_collider.isTrigger = isTrigger;
		_collider.sharedMaterial = material;

		if (hideCollider)
		{
			_collider.hideFlags = HideFlags.HideInInspector;
		}
		else
		{
			_collider.hideFlags = HideFlags.None;
		}
	}

	void Update()
	{

		if (numberOfSides < 4) numberOfSides = 4;
		if (radiusOutside < 0) radiusOutside = 0;
		if (radiusInside < 0) radiusInside = 0;

		if (radiusOutside != radiusOutsideOld || radiusInside != radiusInsideOld || center != centerOld || numberOfSides != numberOfSidesOld)
		{
			points = getPoints(numberOfSides);
			
			_collider.points = points.ToArray();
		}

		if (material != materialOld)
		{
			_collider.sharedMaterial = material;
		}

		if (isTrigger != isTriggerOld)
		{
			_collider.isTrigger = isTrigger;
		}

		materialOld = material;
		radiusOutsideOld = radiusOutside;
		radiusInsideOld = radiusInside;
		isTriggerOld = isTrigger;
		numberOfSidesOld = numberOfSides;
		centerOld = center;
	}

	List<Vector2> getPoints(int precision)
	{
		var points = new List<Vector2>();
		for (int i = 0; i < precision+1; i++)
		{
			points.Add(new Vector2(
				center.x + radiusOutside * Mathf.Cos (-2*Mathf.PI * i/precision),
				center.y + radiusOutside * Mathf.Sin (-2*Mathf.PI * i/precision)
				));
		}
		for (int i = 0; i < precision+1; i++)
		{
			points.Add(new Vector2(
				center.x + radiusInside * Mathf.Cos (2*Mathf.PI * i/precision),
				center.y + radiusInside * Mathf.Sin (2*Mathf.PI * i/precision)
				));
		}
		return points;
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;

		points = getPoints(numberOfSides);

		int j;
		for (int i = 0; i < points.Count; i++)
		{
			if (i == 0) j = points.Count-1;
			else j = i-1;
			Gizmos.DrawLine(
				transform.TransformPoint(new Vector2(points[i].x, points[i].y)),
				transform.TransformPoint(new Vector2(points[j].x, points[j].y))
			);
		}

		//Gizmos.DrawLine
	}
}
