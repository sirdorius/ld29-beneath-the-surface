﻿using UnityEngine;
using System.Collections;

public class CollisionDebugger : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D col)
	{
		//Debug.Log(gameObject.name+" collided with "+col.gameObject.name);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.layer == 9) Debug.Log(gameObject.name+" was triggered by "+col.gameObject.name);
	}
}
