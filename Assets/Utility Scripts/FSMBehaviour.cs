using UnityEngine;
using System;
using System.Collections;

public abstract class FSMBehaviour<StateT> : Behaviour
{
	#region Finite State Machine

	public abstract StateT StartState { get; }

	protected virtual void Start()
	{
		state = StartState;
	}

	protected struct StateTransition
	{
		public StateT oldState;
		public StateT newState;

		public bool Check(StateT oldState, StateT newState)
		{
			return (this.oldState.Equals(oldState)) && (this.newState.Equals(newState));
		}
	}

	[HideInInspector]
	public StateT state;

	public void SwitchState(StateT theNewState)
	{
		var transition = new StateTransition
		{
			oldState = state,
			newState = theNewState
		};

		OnStateEnter(transition);
		state = theNewState;
		OnStateExit(transition);
	}

	protected abstract void OnStateEnter(StateTransition transition);

	protected abstract void OnStateExit(StateTransition transition);

	#endregion
}