using System;
using UnityEngine;

public static class Vector3Extensions
{
	public static Vector2 To2D(this Vector3 vector)
	{
		return new Vector2 (vector.x, vector.y);
	}

	public static void SetXY(this Vector3 vector, float x, float y)
	{
		vector.x = x;
		vector.y = y;
	}
}