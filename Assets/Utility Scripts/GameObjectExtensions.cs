using System;
using UnityEngine;

public static class GameObjectExtensions
{
	public static void SetParent(this GameObject go, GameObject target)
	{
		go.transform.parent = target.transform;
	}

}