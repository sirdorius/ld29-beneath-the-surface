using System;
using UnityEngine;

public static class Vector2Extensions
{
	public static float Project(this Vector2 v, Vector2 vector)
	{
		return Vector2.Dot(v,vector)/vector.magnitude;
	}

	public static Vector2 Rotate(this Vector2 vector, float angle)
	{
		angle *= Mathf.Deg2Rad;
		var x = (float) (vector.x * Math.Cos (angle) - vector.y * Math.Sin (angle));
		var y = (float) (vector.x * Math.Sin (angle) + vector.y * Math.Cos (angle));
		return new Vector2(x,y);
	}

	public static float SignedAngle(Vector2 vector, Vector2 otherVector)
	{
		float result = otherVector.Direction() - vector.Direction();
		if (result < -180f) result += 360f;
		else if (result > 180f) result -= 360f;
		return result;
	}

	public static float SignedAngleTo(this Vector2 vector, Vector2 otherVector)
	{
		return SignedAngle(vector,otherVector);
	}

	public static float Direction(this Vector2 vector)
	{
		float angle = Vector2.Angle(Vector2.right,vector);// Mathf.Acos( Vector2.Dot(vector,otherVector)/(vector.magnitude*otherVector.magnitude) );
		
		return vector.y > 0 ? angle : -angle;
	}

	public static Vector2 WithDirection(this Vector2 vector, float direction)
	{
		return vector.magnitude * Vector2.right.Rotate(direction);
	}

	public static Vector3 To3D(this Vector2 vector)
	{
		return new Vector3(vector.x, vector.y);
	}

	public static Vector3 To3D(this Vector2 vector, float z)
	{
		return new Vector3(vector.x, vector.y, z);
	}

	public static float DistanceTo(this Vector2 vector, Vector2 target)
	{
		return (vector - target).magnitude;
	}
}

