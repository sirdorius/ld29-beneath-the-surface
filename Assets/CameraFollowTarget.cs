﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class CameraFollowTarget : MonoBehaviour {
	public Transform target;
	public float zoom {
		set {
			HOTween.To(camera, 1f, "orthographicSize", value);
		}
		get {
			return camera.orthographicSize;
		}
	}

	void Awake() {
		// HOTween initialization for the whole application
		HOTween.Init(true, false, true);
		HOTween.EnableOverwriteManager();
		HOTween.warningLevel = 0;
	}

	public void FixedUpdate() {
		var t = target.position;
		t.z = -30f;
		HOTween.To(transform, 1f, new TweenParms()
		           .Prop ("position", t));
		HOTween.To(transform, 1f, new TweenParms()
		           .Prop ("rotation", target.rotation));
	}
}
