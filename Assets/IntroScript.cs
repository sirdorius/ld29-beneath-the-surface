﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class IntroScript : MonoBehaviour {
	GameObject player;
	
	void Start() {
		player = GameObject.Find("Player") as GameObject;
	}

	public void targetMainCam() {
		Camera.main.GetComponent<CameraFollowTarget>().target = player.transform;
	}
	public void DestroyCamFollowScript() {
		Camera.main.GetComponent<CameraFollowTarget>().enabled = false;
	}

	public void CamZoom(float level) {
		Camera.main.GetComponent<CameraFollowTarget>().zoom = level;
	}

	public void enablePlayer() {
		var dummy = transform.FindChild("PlayerDummy");
		player.SetActive(true);
		var pos = dummy.position;
		pos.z = player.transform.position.z;
		player.transform.position = pos;
		Destroy(dummy.gameObject);
	}
	
	public void disablePlayer() {
		var p2 = Instantiate(player) as GameObject;
		Destroy(player);
		player = p2;
		p2.SetActive(false);
	}

	public void selfDestruct() {
		transform.Translate(0,0,2);
		Destroy(GetComponent<Animator>());
	}
}
