﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class AnimationManager : MonoBehaviour {
	private Animator animator;
	private MoveActor moveActor;

	void Start() {
		animator = GetComponent<Animator>();
		moveActor = GetComponent<MoveActor>();
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D)) {
			animator.SetTrigger("Bounce");
		}

		if (Input.GetKeyDown(KeyCode.Space)) {
			animator.SetTrigger("Jump");
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		animator.SetFloat("Speed", -moveActor.TangentSpeed);
	}
}
