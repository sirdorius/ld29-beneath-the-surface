﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Door : MonoBehaviour {
	public float factor = 0.8f;
	private Door twinDoor;
	public GameObject prefab;
	public float radDistanceToDoor = 0.2f;

	public void createTwinDoor() {
		Vector3 position = transform.position.x * Vector3.right * factor
			+ transform.position.y * Vector3.up * factor
				+ Vector3.forward * -1f;
		GameObject go =  Instantiate (prefab, position, transform.rotation) as GameObject;
		Door door = go.GetComponent<Door> ();
		setTwinDoor (door);
		door.setTwinDoor (this);
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.layer != LayerMask.NameToLayer ("Player"))
						return;

		/*var newPos = coll.transform.position.normalized * (coll.transform.position.magnitude
			  - (7f) * (transform.position.sqrMagnitude < coll.transform.position.sqrMagnitude ? 1 : -1));

		setColliders(coll.gameObject, false);
		HOTween.To (coll.transform, 1f, new TweenParms()
		            .Prop("position", newPos)
		            .Ease (EaseType.EaseInElastic)
		            .OnComplete(setColliders(coll.gameObject, true))
		);*/

		MoveActor moveActor = coll.GetComponent<MoveActor> ();

		coll.transform.position = getPosition(Angle + radDistanceToDoor * moveActor.Direction);
	
	}

	void setColliders (GameObject go, bool flag) {
		foreach( var g in go.GetComponents<Collider2D>()) {
			g.enabled = flag;
		}
	}

	private float Angle {
		get {
			return Vector3.Angle(Vector3.right, twinDoor.transform.position) * Mathf.Deg2Rad;
		}
	}


	private float Radius {
		get {
			return twinDoor.transform.position.magnitude;
		}
	}

	private Vector3 getPosition (float radiant) {
		float x = Mathf.Cos( radiant * Mathf.PI * 2 ) * Radius;
		float y = Mathf.Sin( radiant * Mathf.PI * 2 ) * Radius;
		return Vector3.right*x + Vector3.up*y + Vector3.forward*-1f;
	}

	public void setTwinDoor(Door door) {
		twinDoor = door;
	}
}
