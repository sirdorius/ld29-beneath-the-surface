﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveActor))]
public class MainCharacterInput : Behaviour {
	public float innerLevel = 9f;
	public float innerLevelJumpNumber = 30f;

	public ButtonMovement leftButton;
	public ButtonMovement rightButton;

	void Awake() {
		Globals.player = gameObject;
	}

	void Update () {
		if (Input.GetButtonDown("Jump"))
		{
			LazyComponent<MoveActor>().OnJump();
		}

		if (transform.position.magnitude < innerLevel && 
		    LazyComponent<MoveActor> ().jumpSpeed != innerLevelJumpNumber) {
			LazyComponent<MoveActor> ().jumpSpeed = innerLevelJumpNumber;
		}

		if (leftButton != null && leftButton.isPressed ()) {
			LazyComponent<MoveActor> ().inputMove = leftButton.getAxis ();
		} else if (rightButton != null && rightButton.isPressed ()) {
			LazyComponent<MoveActor> ().inputMove = rightButton.getAxis ();
		} else {
			LazyComponent<MoveActor> ().inputMove = Input.GetAxisRaw ("Horizontal");

		}


	}

	void customDestroy() {
		Application.LoadLevel (Application.loadedLevel);
	}
}
