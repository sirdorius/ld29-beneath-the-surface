﻿using UnityEngine;
using System.Collections;

public class IceBomb : Damage {
	void Awake() {
		destroyMask = 1<<9 | 1 << 8 | 1 << 10;
		explosionGO = Resources.Load("KamikazeExplosion") as GameObject;
	}
}