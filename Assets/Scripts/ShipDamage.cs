﻿using UnityEngine;
using System.Collections;

public class ShipDamage : Damage {

	protected override void OnCollisionEnemy(Damageable dam)
	{
		if (dam.name.StartsWith ("Bomber")) return;
		dam.takeDamage (1000f);
	}

	protected override void customDestroy() {
	}
}
