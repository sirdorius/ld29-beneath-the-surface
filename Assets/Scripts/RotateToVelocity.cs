﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Plugins;

public class RotateToVelocity : MonoBehaviour {
	public float offset;
	// Update is called once per frame
	void FixedUpdate () {
		var angle = Mathf.Atan2(rigidbody2D.velocity.y, rigidbody2D.velocity.x) * Mathf.Rad2Deg;
		transform.localEulerAngles = new Vector3(0,0,angle+offset);
	}
}
