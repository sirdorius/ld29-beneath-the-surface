﻿using UnityEngine;
using System.Collections;


public class EnemySpawner : MonoBehaviour {
	public GameObject[] prefabs;
	public int[] numbers;
	public float radius = 20f;

	private GameObject[] enemies;
	private int numEnemies;
	private bool deactivateOnStart = true;

	public float deltaRadiant;

	/* public GameObject kamikazePrefab;
	public GameObject bomberPrefab;
	public GameObject SlugPrefab;
	public GameObject iceScreamPrefab; */

	private Vector3 getWorldPosition(float value) {
		float x = Mathf.Cos( value * Mathf.PI * 2 ) * radius;
		float y = Mathf.Sin( value * Mathf.PI * 2 ) * radius;
		return Vector3.right*x + Vector3.up*y + Vector3.forward*-1f;
	}

	public float getFraction(float value) {
		return value * Mathf.PI*2;
	}

	public float randomDelta {
		get {
			return (Random.value*2f-0.5f)*deltaRadiant;
		}
	}

	public bool onSameLevel(GameObject player) {
		float distance = player.transform.position.y - radius;
		return distance>-3f && distance < 10f;
	}

	void Start() {
		numEnemies = 0;
		for (int i=0; i<prefabs.Length; i++) {
			numEnemies += numbers[i];
		}

		enemies = new GameObject[numEnemies];

		for(int c=0, j=0; c<prefabs.Length; c++) {
			for(int i=0; i<numbers[c]; i++,j++) {
				float fraction = c;

				GameObject enemy = Instantiate(prefabs[c], getWorldPosition(getFraction(i)+randomDelta), Quaternion.identity) as GameObject;
				enemy.transform.parent = transform;
				enemy.SetActive(false);
				enemies[j] = enemy;
			}
		}

		int randomDoorMan = Random.Range (0, numEnemies);
		enemies[randomDoorMan].AddComponent<DoorMan>();
	}

	void Update() {
		if(deactivateOnStart) {
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			if(player!=null && onSameLevel(player)) {
				for(int i=0; i<enemies.Length; i++) {
					enemies[i].SetActive(true);
				}
				deactivateOnStart = false;
			}
		}
	}
}
