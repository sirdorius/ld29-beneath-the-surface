﻿using UnityEngine;
using System.Collections;

public class GravityPull : MonoBehaviour {

	public float gravityForce = 20f;

	void FixedUpdate()
	{
		var position = transform.GetPosition2D();

		rigidbody2D.AddForce(- gravityForce * position.normalized);
	}
}
