﻿using UnityEngine;
using System.Collections;

public class KamikazeExplosion : MonoBehaviour {
	public float decreaseSpeedOf = 5.5f;
	private MoveActor moveActor;

	void OnTriggerStay2D(Collider2D coll) {
		audio.Play ();
		moveActor = coll.GetComponent<MoveActor> ();
		if (moveActor != null) {
			moveActor.decreaseSpeed (decreaseSpeedOf);
		}
	}
}
