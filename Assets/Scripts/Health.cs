﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
	public float health;

	public void getDemage(float value) {
		health -= value;
		if (health <= 0) {
			Destroy(gameObject);
		}
	}
}
