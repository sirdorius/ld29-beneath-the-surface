using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Harpoon : Damage {
	void Awake() {
		destroyMask = 1 << 9;
	}

	protected override void customDestroy ()
	{
		foreach (Component c in gameObject.GetComponents<Component>()) {
			if (c.GetType() != typeof(Transform)
			    && c.GetType() != typeof(SpriteRenderer)
			    && c != this)
				Destroy(c);
		}
		StartCoroutine(destroyAfter(10f));
	}

	protected override void OnCollisionEnemy(Damageable dam) {
		base.OnCollisionEnemy (dam);

		if (dam.divideBullet) {
			var num = 2f;
			var angleSpread = 30f;
			print ("divided 1");
			for (int i = 1; i<2; i++) {
				GameObject copy = Instantiate(gameObject) as GameObject;
				copy.transform.Rotate2D(angleSpread * i
				                            * ((rigidbody2D.velocity.x>0) ? 1 : -1));
				print ("divided");
			}
		}

		if (dam.power == "") return;

		rigidbody2D.velocity = dam
			.outVector(rigidbody2D.velocity);
		Damage newPower = gameObject.AddComponent(dam.power) as Damage;

		newPower.creator = dam.gameObject;
		var effect = new GameObject();
		var sprite = effect.AddComponent("SpriteRenderer") as SpriteRenderer;
		sprite.sprite = dam.graphicEffectOfPower;
		effect.transform.parent = transform;
		effect.transform.localPosition = Vector3.zero;
		effect.transform.localEulerAngles = Vector3.zero;
	}

	IEnumerator destroyAfter(float time) {
		yield return new WaitForSeconds(time);
		var t = GetComponent<SpriteRenderer>();
		var cFinal = t.color;
		cFinal.a = 0;
		HOTween.To(t, 2f, "color", cFinal);
		yield return new WaitForSeconds(2f);
		Destroy (gameObject);
	}
}
