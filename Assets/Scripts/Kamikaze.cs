﻿using UnityEngine;
using System.Collections;

public class Kamikaze : Damage {
	void Awake() {
		destroyMask = 1 << 8;
		explosionGO = Resources.Load ("KamikazeExplosion") as GameObject;
	}

	protected override void customDestroy ()
	{
		DoorMan doorMan = GetComponent<DoorMan> ();
		if (doorMan != null)
						doorMan.customDestroy ();
		base.customDestroy ();
	}
}
