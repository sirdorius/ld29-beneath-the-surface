﻿using UnityEngine;
using System.Collections;

public class MoveActor : Behaviour {

	#region Inspector fields

	public float speed = 5f;
	public float recoverySpeedOf = 0.1f;
	
	private float targetSpeed;
	private float maxSpeed;

	private int jumpsLeft;

	public bool grounded;

	public int numberOfJumps = 1;
	public AudioClip jumpAudio;

	public float acceleration = 1000f;

	public float epsilon = 1f;

	public float jumpSpeed = 5;

	public bool debug = false;

	public float inputMove;

	#endregion

	void Start() {
		maxSpeed = speed;
	}

	void FixedUpdate()
	{
		DoMove();

		DoCheckGrounded();

		if (debug)
			Debug.Log ("Normal: "+NormalVector.Direction()+" Tangent: "+TangentVector.Direction()+" NormalSpeed: "+NormalSpeed+" TangentSpeed: "+TangentSpeed);
	}

	void DoMove()
	{
		targetSpeed = -inputMove * maxSpeed;
		if(maxSpeed < speed) {
			maxSpeed += recoverySpeedOf;
			if(maxSpeed>speed) maxSpeed = speed;
		}

		if (Mathf.Abs(targetSpeed - TangentSpeed) < epsilon)
		{
			TangentSpeed = targetSpeed;
		}
		else
		{
			if (TangentSpeed < targetSpeed)
			{
				ApplyTangentForce(acceleration * Time.fixedDeltaTime);
			}
			else if (TangentSpeed > targetSpeed)
			{
				ApplyTangentForce(-acceleration * Time.fixedDeltaTime);
			}
		}
	}

	void DoCheckGrounded()
	{
		int layer = 9;

		var pos = transform.GetPosition2D();
		var radius = LazyComponent<CircleCollider2D>().radius;

		RaycastHit2D hit = Physics2D.Raycast(pos, NormalVector, (radius  + 0.05f), 1 << layer);

		if (hit != null)
		{
			float distance = hit.point.DistanceTo(transform.GetPosition2D());
			if (distance < radius + 0.05f)
			{
				jumpsLeft = numberOfJumps;
				grounded = true;

			}
			else
			{
				grounded = false;
			}
		}
		else
		{
			grounded = false;
		}
	}

	public void decreaseSpeed(float value) {
		if (speed > 0) {
			maxSpeed -= value;
			if(maxSpeed<0) maxSpeed = 0; 
		}
	}

	public void OnJump()
	{
		if (grounded || jumpsLeft > 1)
		{
			audio.clip = jumpAudio;
			audio.Play();
			NormalSpeed = -jumpSpeed;
			jumpsLeft --;
		}
	}

	#region Normal and Tangent vector manipulation

	public float Direction {
		get {
			return Mathf.Sign(inputMove);
		}
	}

	Vector2 NormalVector
	{
		get
		{
			return -transform.GetPosition2D().normalized;
		}
	}

	Vector2 TangentVector
	{
		get
		{
			return NormalVector.Rotate(-90f).normalized;
		}
	}

	public float TangentSpeed
	{
		get
		{
			return rigidbody2D.velocity.Project(TangentVector);
		}

		set
		{
			rigidbody2D.velocity = NormalSpeed * NormalVector + value * TangentVector;
		}
	}

	float NormalSpeed
	{
		get
		{
			return rigidbody2D.velocity.Project(NormalVector);
		}
		set
		{
			rigidbody2D.velocity = value * NormalVector + TangentSpeed * TangentVector;
		}
	}

	void ApplyTangentForce(float force)
	{
		rigidbody2D.AddForce(TangentVector * force);
	}

	void ApplyNormalForce(float force)
	{
		rigidbody2D.AddForce(NormalVector * force);
	}

	#endregion
}
