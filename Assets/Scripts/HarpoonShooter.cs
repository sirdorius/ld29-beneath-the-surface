﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class HarpoonShooter : MonoBehaviour {
	public float force;
	public float rechargeTime;
	[HideInInspector]
	public bool canShoot = true;
	public AudioClip sound;
	private GameObject gunSpawn;
	Tweener weaponsShootTween;

	private GameObject gunGO;

	void Start() {
		canShoot = true;
		gunGO = transform.FindChild("HarpoonGun").gameObject;
		gunSpawn = transform.FindChild("HarpoonGun/Spawner").gameObject;
	}

	// Update is called once per frame
	void Update () {
		Vector3 pointer;
#if UNITY_STANDALONE || UNITY_EDITOR
		pointer = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		setGunPosition (pointer);
		if(canShoot && Input.GetMouseButton(0)) {
			shoot("Harpoon", pointer);

			StartCoroutine(recharge());
			Animation();
		}
#endif

		foreach(Touch touch in Input.touches) {
			if(touch.position.y > Screen.width/100*20f && canShoot) {
				pointer = Camera.main.ScreenToWorldPoint(touch.position);
				setGunPosition(pointer);

				shoot("Harpoon",pointer);
				StartCoroutine(recharge());
				Animation();
			}
		}


	}

	void setGunPosition (Vector3 pointer) {
		pointer = pointer-transform.position;
		var angle = Mathf.Atan2(pointer.y, pointer.x) * Mathf.Rad2Deg;
		gunGO.transform.eulerAngles = new Vector3(0,0,angle+90);
	}

	void shoot(string prefabName, Vector3 position) {
		audio.clip = sound;
		audio.Play ();

		var go = Instantiate(Resources.Load(prefabName)) as GameObject;
		var damage = go.GetComponent<Damage>();
		if (damage)
			damage.creator = gameObject;
		position.z = 0;
		go.transform.position = gunSpawn.transform.position;
		go.rigidbody2D.AddForce((position-transform.position).normalized * force);
	}

	void Animation() {
		if (weaponsShootTween != null) weaponsShootTween.Complete();
			weaponsShootTween = HOTween.From (transform.FindChild("HarpoonGun"), 1f, new TweenParms()
			                               .Prop ("localScale", new Vector3(-1f,1.3f,0))
			                               .Ease(EaseType.EaseOutElastic));
		//animator.SetTrigger("Shoot");
	}

	IEnumerator recharge() {
		canShoot = false;
		yield return new WaitForSeconds(rechargeTime);
		canShoot = true;
	}
}
