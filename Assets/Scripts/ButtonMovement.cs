﻿using UnityEngine;
using System.Collections;

public class ButtonMovement : MonoBehaviour {
	public bool right;
	private float movement;

	void OnMouseDown () {
		if (right) {
			movement = 1;
		}
		else
			movement = -1;
	}

	void OnMouseUp() {
		movement = 0;
	}

	public bool isPressed() {
		return movement != 0;
	}

	public float getAxis() {
		return movement;
	}
}
