using UnityEngine;
using System.Collections;

public class JellyFish : MonoBehaviour {
	public int maxCopy = 4;
	public int numCopy = 2;
	public GameObject prefab;
	public float factorSize = 0.6f;

	void OnTriggerEnter2D(Collider2D coll) {
		string name = coll.name;
		if (name.Contains ("JellyFish") || name.Contains("Level"))
						return;
		GameObject go;

		int n = 0;
		for (; n < numCopy && n < maxCopy; n++) {
			Vector3 position = transform.position - n * Vector3.right* (transform.localScale.x + 1f);

			go = Instantiate(Resources.Load("JellyFish"), position, transform.rotation) as GameObject;

			go.transform.localScale = transform.localScale * factorSize;

			JellyFish copy = go.GetComponent<JellyFish>();
			copy.maxCopy = maxCopy/numCopy;
		}

		DoorMan doorMan = GetComponent<DoorMan> ();
		if (doorMan != null) {
			doorMan.customDestroy();
		}

		Destroy (gameObject);
	}
}
