﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveActor))]
public class FollowPlayer : Behaviour {

	private GameObject player {
		get {
			return Globals.player;
		}
	}
	public float eulerAngleMaxFollow = 20f;
	public float eulerAngleMinFollow = 5f;
	public float sameLevelEpsilonUp = 3f;
	public float sameLevelEpsilonDown = 3f;

	float playerAngularDistance
	{
		get
		{
			return Vector2Extensions.SignedAngle(transform.GetPosition2D(), player.transform.GetPosition2D());
		}
	}

	bool IsOnSameLevel {
		get {
			if (player)
				return transform.position.magnitude - player.transform.position.magnitude < sameLevelEpsilonUp &&
					transform.position.magnitude - player.transform.position.magnitude > sameLevelEpsilonDown ;
			else
				return false;
		}
	}

	void Update()
	{
		//Debug.Log(playerAngularDistance);
		LazyComponent<MoveActor> ().inputMove = 0f;
		if (!IsOnSameLevel) return;
		if (Mathf.Abs (playerAngularDistance) < eulerAngleMaxFollow
		    && Mathf.Abs (playerAngularDistance) > eulerAngleMinFollow)
			LazyComponent<MoveActor> ().inputMove = (playerAngularDistance > 0f) ? -1f : 1f;
	}
}
