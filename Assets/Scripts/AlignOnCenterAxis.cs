using UnityEngine;
using System.Collections;

public class AlignOnCenterAxis : MonoBehaviour {
	void Start() {
		StartCoroutine(doUpdate());
	}
	// Update is called once per frame
	IEnumerator doUpdate () {
		transform.localEulerAngles = new Vector3(0,0
			, Mathf.Atan2(transform.position.y, transform.position.x)*Mathf.Rad2Deg-90);
		/*transform.LookAt2D(Vector2.zero);
		transform.Rotate2D(90);*/
		yield return new WaitForSeconds(0.33f);
		StartCoroutine(doUpdate());
	}
}
