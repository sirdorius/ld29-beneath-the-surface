﻿using UnityEngine;
using System.Collections;

public class FixedSpawner : MonoBehaviour {
	public GameObject prefab;
	public float radiant;
	public float radius = 20f;


	private GameObject instance;

	private Vector3 FixedPosition{
		get {
			float x = Mathf.Cos( radiant * Mathf.PI * 2 ) * radius;
			float y = Mathf.Sin( radiant * Mathf.PI * 2 ) * radius;
			return Vector3.right*x + Vector3.up*y + Vector3.forward*-1f;
		}
	}

	void Start() {
		instance = Instantiate(prefab, FixedPosition, Quaternion.identity) as GameObject;
	}
	/*void Update() {
		instance.transform.position = FixedPosition;
	}*/
}
