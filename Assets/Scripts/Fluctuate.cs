﻿using UnityEngine;
using System.Collections;

public class Fluctuate : MonoBehaviour {
	public float gravityForce = 20.05f;
	public float height = 4f;
	public bool debug;
	
	void FixedUpdate()
	{
		int layer = 10;
		
		var pos = transform.GetPosition2D();

		RaycastHit2D hit = Physics2D.Raycast(pos, -pos.normalized, (height  + 0.05f),
		                                     1<<9);
		
		if (hit != null)
		{
			float distance = hit.point.DistanceTo(transform.GetPosition2D());
			if(debug) {
				print ("distance: "+distance+" point:"+hit.point+" pos:"+
				       transform.position+" frac:"+hit.fraction+" name:"+hit.transform.name);
			}
			if (distance < height + 0.05f)
			{
				rigidbody2D.AddForce(gravityForce * pos.normalized);
			}
		}
	}
}

