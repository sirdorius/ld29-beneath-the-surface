﻿using UnityEngine;
using System.Collections;

public class DoorMan : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject  key = Resources.Load("Key") as GameObject;
		key = Instantiate (key) as GameObject;
		key.transform.parent = transform;
		key.transform.localPosition = new Vector3(0,3f,0);
	}

	public void customDestroy() {
		GameObject  doorPrefab = Resources.Load("Door") as GameObject;
		GameObject go = Instantiate (doorPrefab, transform.position, transform.rotation) as GameObject;
		Door door = go.GetComponent<Door> ();
		door.createTwinDoor ();
	}
}
