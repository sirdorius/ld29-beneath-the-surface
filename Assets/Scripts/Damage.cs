﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Damage : MonoBehaviour {
	public float damageAmout;
	public bool startAnimation = false;
		public LayerMask destroyMask = 1<<9 | // Player
			1<<8 | // World
			1<<10; // Enemies
	[HideInInspector] public GameObject creator;

	protected GameObject explosionGO;

	void Start() {
		if (startAnimation) {
			HOTween.From (transform, 0.2f, new TweenParms ()
		             .Prop ("localScale", Vector3.zero));
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (!coll.isTrigger || coll.gameObject == creator) return;

		var dam = coll.GetComponent<Damageable>();

		if (coll.gameObject.layer == LayerMask.NameToLayer ("Player") && dam != null) {
			dam.takeDamage(damageAmout);
		}

		if (coll.gameObject.layer == LayerMask.NameToLayer("Enemies") && dam != null) {
			dam.takeDamage(damageAmout);
			OnCollisionEnemy(dam);
		}

		/*
		 * Destroy bullet
		 */
		if (((1 << coll.gameObject.layer) & destroyMask) != 0) {
			if (explosionGO) {
				Instantiate(explosionGO, transform.position, Quaternion.identity);
			}
			customDestroy();
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		if (coll.gameObject == creator) {
			creator = null;
		}
	}

	virtual protected void OnCollisionEnemy(Damageable dam) {
		creator = dam.gameObject;
	}

	virtual protected void customDestroy() {
		if (GetComponents<Damage>().Length == 1)
			Destroy (gameObject);
	}
}
