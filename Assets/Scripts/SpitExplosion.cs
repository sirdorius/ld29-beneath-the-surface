﻿using UnityEngine;
using System.Collections;

public class SpitExplosion : MonoBehaviour {
	public float dps;

	void OnTriggerStay2D(Collider2D coll) {
		if (!coll.isTrigger || coll.GetComponent<Shooter>() != null) return;
		Damageable health = coll.gameObject.GetComponent<Damageable> ();
		if (health) {
			health.takeDamage(dps*Time.deltaTime);
		}
	}
}
