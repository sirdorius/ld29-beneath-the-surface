using UnityEngine;
using System.Collections;

public class Bomb : Damage {
	void Awake() {
		if(transform.parent!=null) destroyMask = 1<<8  | 1<<10; // Enemies
		explosionGO = Resources.Load ("BombExplosion") as GameObject;
	}

	void Update() {
		if (transform.parent != null) {
			transform.localPosition = - Vector2.up*2f;
		}
	}

	public void shoot() {
		destroyMask = 1<<8 | 1<<10 | 1<<9 ;
		transform.parent = null;
		rigidbody2D.isKinematic = false;
		GetComponent<GravityPull> ().enabled = true;
	}
}
