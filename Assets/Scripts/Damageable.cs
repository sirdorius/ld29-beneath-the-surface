﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Damageable : MonoBehaviour {
	public float health = 100f;
	public string power;
	public GameObject bloodPrefab;
	public bool divideBullet = false;
	public Sprite graphicEffectOfPower;

	public bool invertVelocityXOnExit, invertVelocityYOnExit;
	public float angleOffsetOnExit = 0f; // in direction of normal

	public AudioClip audioHit;
	public AudioClip audioDeath;

	private float dampen = 0.7f;
	private LifeBar lifeBar;

	private bool isDead = false;

	void Start() {
		var t = Instantiate(Resources.Load("LifeBar")) as GameObject;
		lifeBar = t.GetComponent<LifeBar>();
		lifeBar.transform.parent = transform;
		lifeBar.transform.localPosition = new Vector3(0, 1, -1.5f);
		lifeBar.transform.localEulerAngles = Vector3.zero;
	}

	public Vector2 outVector(Vector2 inVector) {
		Vector2 localDir = transform.InverseTransformDirection(inVector);
		localDir.x *= invertVelocityXOnExit ? -1 : 1;
		localDir.y *= invertVelocityYOnExit ? -1 : 1;
		var dir = localDir.x < 0 ? -1 : 1;
		return transform.TransformDirection(localDir.Rotate(dir * angleOffsetOnExit)) * dampen;
	}

	public void takeDamage(float value) {
		health -= value;
		if (health <= 0) {
			if(value>0 && bloodPrefab != null) {
				var bloodPS = Instantiate(bloodPrefab) as GameObject;
				bloodPS.transform.position = transform.position;
			}

			if(audioDeath!=null) {
				audio.clip = audioDeath;
				audio.Play();
			}
			SendMessage("customDestroy");
		}
		else {
			if(audioHit!=null) {
				audio.clip = audioHit;
				audio.Play();
			}
		}
	}

	void customDestroy() {
		if (!isDead) {
			isDead = true;
			HOTween.To (transform, 0.5f, new TweenParms ()
		           .Prop ("localScale", Vector3.zero)
		           .Ease (EaseType.EaseInOutElastic)
		           .OnComplete (() => Destroy (gameObject)));
		}
	}
}
