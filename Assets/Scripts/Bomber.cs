﻿using UnityEngine;
using System.Collections;

public class Bomber : Behaviour {
	public GameObject bombPrefab;
	public float countdown = 3f;
	public float timeNewBomb = 1f;
	private Bomb bombInstance;
	public float shootAngle = 3f;
	public float sameLevelEpsilonUp = 3f;
	public float sameLevelEpsilonDown = 3f;

	private GameObject player {
		get {
			return Globals.player;
		}
	}

	private float time;

	// Use this for initialization
	void Start () {
		MoveActor moveActor = LazyComponent<MoveActor> ();
		moveActor.inputMove = 0.5f;
		createBomb ();
	}

	void Update() {
		if(bombInstance==null) {
			time += Time.deltaTime;
			if(time > countdown) {
				createBomb();
				time = 0;
			}
		}
		else if (Mathf.Abs (playerAngularDistance) < shootAngle && IsOnSameLevel) {
			time += Time.deltaTime;
			if(time > countdown) {
				bombInstance.shoot ();
				time = 0;
			}
		}
	}

	bool IsOnSameLevel {
		get {
			return transform.position.magnitude - player.transform.position.magnitude < sameLevelEpsilonUp &&
				transform.position.magnitude - player.transform.position.magnitude > sameLevelEpsilonDown ;
		}
	}

	float playerAngularDistance
	{
		get
		{
			return Vector2Extensions.SignedAngle(transform.GetPosition2D(), player.transform.GetPosition2D());
		}
	}

	private void createBomb() {
		GameObject bomb = Instantiate (bombPrefab) as GameObject;
		bomb.transform.parent = transform;
		bomb.transform.localPosition = new Vector2(0,-2f);
		bombInstance = bomb.GetComponent<Bomb>();
		bombInstance.transform.parent = transform;
		bombInstance.creator = gameObject;
	}
}
