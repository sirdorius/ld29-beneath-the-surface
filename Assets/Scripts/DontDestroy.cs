﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {
	public string nameLevel = "Test";

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
	
	}

	void OnLevelWasLoaded(int level) {
		if (Application.loadedLevelName == nameLevel)
						Destroy (gameObject);
	}
}
