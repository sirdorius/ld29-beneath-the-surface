﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Damageable))]
public class LifeBar : MonoBehaviour {
	float initLife;
	float life;
	public Damageable target;
	Transform bar;
	SpriteRenderer barSprite;
	public Color lifeMax = new Color(0.26f,1,0.57f);
	public Color lifeMed = new Color(1,1,0.26f);
	public Color lifeLow = new Color(1,0.26f,0.26f);

	// Use this for initialization
	void Start () {
		bar = transform.Find("bar");
		barSprite = transform.Find ("bar/bar").GetComponent<SpriteRenderer>();
		target = transform.parent.GetComponent<Damageable>();
		initLife = target.health;
	}
	
	// Update is called once per frame
	void Update () {
		if (target.health != life) {
			life = target.health;
			if (life <= 0) Destroy (gameObject);
			bar.localScale = new Vector3(life/initLife,1,1);
			if (life > initLife/2)
				barSprite.color = Color.Lerp(lifeMax, lifeMed, (initLife-life)/(initLife/2f));
			else
				barSprite.color = Color.Lerp(lifeMed, lifeLow, (initLife/2f-life)/(initLife/2f));
		}
	}
}
