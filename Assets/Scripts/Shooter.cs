﻿using UnityEngine;
using System.Collections;

public class Shooter : Behaviour {
	private GameObject player {
		get {
			return Globals.player;
		}
	}
	public float stayAway = 5f;
	private bool cooldown = false;
	public float sameLevelEpsilon = 3f;

	public float cooldownTime = 3f;

	public GameObject bullet;

	bool IsOnSameLevel {
		get {
			return Mathf.Abs(player.transform.position.magnitude-transform.position.magnitude)<sameLevelEpsilon;
		}
	}

	public float shootForce = 1000f;

	float playerAngularDistance
	{
		get
		{
			if (player)
				return Vector2Extensions.SignedAngle(transform.GetPosition2D(), player.transform.GetPosition2D());
			else
				return 0f;
		}
	}

	IEnumerator Cooldown () {
		yield return new WaitForSeconds(cooldownTime);
		cooldown = false;
	}

	void Update () {
		if (Mathf.Abs (playerAngularDistance) < stayAway + 0.5f && !cooldown && IsOnSameLevel) {
			var go = Instantiate(bullet) as GameObject;

			Damage damage = go.GetComponent<Damage>();
			damage.creator = gameObject;
			go.transform.position = transform.position;

			setDirection(go);

			cooldown = true;
			StartCoroutine(Cooldown());
		}
	}

	private void setDirection(GameObject go) {
		var direction = transform.up.To2D().Rotate(Mathf.Sign(playerAngularDistance)*45);
		var playerDistance = (player.transform.position - transform.position).magnitude;
		go.rigidbody2D.AddForce(direction.normalized * (playerDistance/10f) * shootForce);
	}
}
